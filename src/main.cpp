#include "main.hpp"

void mainFunc(hls::stream<pkt>& stringStream, hls::stream<pkt>& queryStream, hls::stream<outputPkt>& outputStream) {
#pragma HLS INTERFACE axis port=stringStream register_mode=both name=strStream register
#pragma HLS INTERFACE axis port=queryStream register_mode=both name=qryStream register
#pragma HLS INTERFACE axis port=outputStream register_mode=both name=outStream register
#pragma HLS LOOP_MERGE

	word stringWbW[stringLengthMaxWords];
	for (int i = 0; i < stringLengthMaxWords; i++) {
		stringWbW[i] = 0;
	}
#pragma HLS ARRAY_PARTITION variable=stringWbW type=complete

	string_packets_loop:for (int i = 0; i < expectedPackets; i++) {
		pkt p1 = stringStream.read();
		internal_string_packets_loop:for (int j = 0; j < wordsInBuffer; j++) {
			stringWbW[i * wordsInBuffer + j] = p1.data[j];
		}
	}

	scoreUint scores[wordsInBuffer + 1][stringLengthMaxWordsPlus1];
//#pragma HLS ARRAY_PARTITION variable=scores type=complete dim=0
	zero_row_loop:for (int i = 0; i < wordsInBuffer + 1; i++) {
		for (int j = 0; j < stringLengthMaxWordsPlus1; j++) {
			scores[i][j] = 0;
		}
	}

	scorePosVec hValues[expectedPackets][highestNumberItrs + 1];
	query_packets_loop:for (int i = 0; i < expectedPackets; i++) {
//#pragma HLS PERFORMANCE target_ti=1
#pragma HLS PIPELINE II=1
		pkt p1 = queryStream.read();
		calcRows(hValues[i][0], scores, stringWbW, p1.data, i * wordsInBuffer);
	}
	outputPkt highScorePkt;
	highest_loop:for (int i = 0; i < expectedPackets; i++) {
#pragma HLS PIPELINE II=1
		highest_itr_loop:for (int j = 0; j < highestNumberItrs; j++) {
			for (int k = 0; k < wordsInBuffer; k = k + cPow2(j + 1)) {
#pragma HLS UNROLL
				int power = cPow2(j);
				if (hValues[i][j][k].score < hValues[i][j][k + power].score) {
					hValues[i][j + 1][k] = hValues[i][j][k + power];
				}
				else {
					hValues[i][j + 1][k] = hValues[i][j][k];
				}
			}
		}
		if (hValues[i][highestNumberItrs][0].score > highScorePkt.data.score) {
			highScorePkt.data = hValues[i][highestNumberItrs][0];
		}
	}

	highScorePkt.last = 1;
	outputStream.write(highScorePkt);
    return;

//    highest_loop:for (int i = 0; i < expectedPackets; i++) {
//		highest_itr_loop:for (int k = 0; k < highestNumberItrs; k++) {
//#pragma HLS PIPELINE II=1
//			scorePosVec score = hValueStreams[k].read();
//			internal_highest_loop:for (int l = 0; l < wordsInBuffer; l = l + cPow2(k + 1)) {
////#pragma HLS UNROLL
//				int power = cPow2(k);
////				std::cout << "Score and score: " << score[l].score << " : " << score[l + power].score << " with l: " << l << std::endl;
//				if (score[l].score < score[l + power].score) {
//					score[l] = score[l + power];
//				}
//			}
//			hValueStreams[k + 1].write(score);
//		}
//		outputPkt outPkt;
//		outPkt.data = hValueStreams[highestNumberItrs].read()[0];
//		std::cout << "Tha big data: " << outPkt.data.score << std::endl;
//		if (outPkt.data.score > highScorePkt.data.score) {
//			highScorePkt.data = outPkt.data;
//		}
//	}

}


