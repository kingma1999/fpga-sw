#include "scoreCalc.hpp"

void LTCalc(scoreInt& retScore, scoreUint& LTScore, word queryW, word stringW) {
#pragma HLS inline
//	std::cout << "Bruh " << LTScore;
	if (stringW == queryW) {
		retScore = LTScore + subScore;
	}
	else if ((scoreInt) LTScore - subScore > retScore) {
		retScore = LTScore - subScore;
	}
}

void LCalc(scoreInt& retScore, scoreUint& LScore) {
#pragma HLS inline
	scoreInt temp = (scoreInt) LScore - gapPenalty;
	if (temp > retScore) {
		retScore = temp;
	}
}
void TCalc(scoreInt& retScore, scoreUint& TScore) {
#pragma HLS inline
	scoreInt temp = (scoreInt) TScore - gapPenalty;
	if (temp > retScore) {
		retScore = temp;
	}
}

scoreUint scoreCalc(scoreUint& LScore, scoreUint& TScore, scoreUint& LTScore, word queryW, word stringW) {
#pragma HLS inline
//#pragma HLS LATENCY max=1
//#pragma HLS DATAFLOW
	scoreInt score = 0;
	LTCalc(score, LTScore, queryW, stringW);
	LCalc(score, LScore);
	TCalc(score, TScore);
	return (scoreUint) score;
}

scoreInt largestNumber(scoreInt score1, scoreInt score2) {
#pragma HLS INLINE
	if (score1 > score2) {
		return score1;
	} else {
		return score2;
	}
}
