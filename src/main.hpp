#ifndef MAIN_HPP
#define MAIN_HPP

#include <iostream>
#include <stdio.h>
#include <fstream>
#include <vector>

// Design files
#include "constants.hpp"
#include "calcRow.hpp"

void mainFunc(hls::stream<pkt>& stringStream, hls::stream<pkt>& queryStream, hls::stream<outputPkt>& outputStream);

#endif // MAIN_HPP
