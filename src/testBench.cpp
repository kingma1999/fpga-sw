//string = 0b101011111000011100 # GGTTGACTA (2,2,3,3,2,0,1,3,0)
//query  = 0b1110111100011010 # TGTTACGG (3,2,3,3,0,1,2,2)

#include <iostream>
#include <cstdlib>
#include "main.hpp"

word stringWbW[stringLengthMaxWords];
word queryWbW[stringLengthMaxWords];

// void scoreCalcArr(scoreUint& retScore, scoreUint& LScore, scoreUint& TScore, scoreUint& LTScore, word queryW, word stringW);

void unitTest() {
	scoreUint retScore = 0, LScore, TScore, LTScore;
	word queryW, stringW;
	LScore = 1;
	TScore = 1;
	LTScore = 3;
	queryW = 1;
	stringW = 1;
//	scoreCalcArr(retScore, LScore, TScore, LTScore, queryW, stringW);
	std::cout << "Expected score: 6 , actual score: " << retScore << std::endl;
}

scorePosition referenceSW(int stringWbW[stringLengthMaxWords], int queryWbW[stringLengthMaxWords]) {
	scorePosition highScore;
	int scoreMatrix[stringLengthMaxWordsPlus1][stringLengthMaxWordsPlus1];
	for (int i = 0; i < stringLengthMaxWordsPlus1; i++) {
		for (int j = 0; j < stringLengthMaxWordsPlus1; j++) {
			scoreMatrix[i][j] = 0;
		}
	}
	for (int i = 1; i < stringLengthMaxWordsPlus1; i++) {
#pragma HLS PIPELINE II=1
		for (int j = 1; j < stringLengthMaxWordsPlus1; j++) {
#pragma HLS PIPELINE II=1
			if (stringWbW[i - 1] == queryWbW[j - 1]) {
				scoreMatrix[i][j] = scoreMatrix[i - 1][j - 1] + subScore;
			}
			else {
				scoreMatrix[i][j] = scoreMatrix[i -1][j - 1] - subScore;
			}
			if ((scoreMatrix[i - 1][j] - gapPenalty) > scoreMatrix[i][j]) {
				scoreMatrix[i][j] = scoreMatrix[i - 1][j] - gapPenalty;
			}
			if ((scoreMatrix[i][j - 1] - gapPenalty) > scoreMatrix[i][j]) {
				scoreMatrix[i][j] = scoreMatrix[i][j - 1] - gapPenalty;
			}
			if (scoreMatrix[i][j] < 0) {
				scoreMatrix[i][j] = 0;
			}
			if (scoreMatrix[i][j] > highScore.score) {
				highScore.score = scoreMatrix[i][j];
				highScore.row = i;
				highScore.column = j;
			}
//			std::cout << scoreMatrix[i][j] << " | ";
		}
//		std::cout << std::endl;
	}
	return highScore;
}

void completeFunctionality() {
	hls::stream<pkt> stringStream;
	hls::stream<pkt> queryStream;
	hls::stream<outputPkt> outputStream;

	queryWbW[0] = 2;
	queryWbW[1] = 2;
	queryWbW[2] = 3;
	queryWbW[3] = 3;
	queryWbW[4] = 2;
	queryWbW[5] = 0;
	queryWbW[6] = 1;
	queryWbW[7] = 3;
	queryWbW[8] = 0;
	int queryLength = 9;

	stringWbW[0] = 3;
	stringWbW[1] = 2;
	stringWbW[2] = 3;
	stringWbW[3] = 3;
	stringWbW[4] = 0;
	stringWbW[5] = 1;
	stringWbW[6] = 2;
	stringWbW[7] = 2;
	stringWbW[8] = 1;
	int stringLength = 8;
	srand(time(NULL));

	for (int i = 0; i < stringLengthMaxWords; i++) {
		queryWbW[i] = (word) (rand());
		stringWbW[i] = (word) (rand());
	}

	for (int i = 0; i < expectedPackets; i++) {
		pkt Pkt;
		for (int j = 0; j < wordsInBuffer; j++) {
			Pkt.data[j] = stringWbW[i * wordsInBuffer + j];
			if (i == expectedPackets - 1) {
				Pkt.last = 1;
			} else { Pkt.last = 0;}
			std::cout << Pkt.data[j];
		}
		stringStream.write(Pkt);
	}
	std::cout << std::endl;

	for (int i = 0; i < expectedPackets; i++) {
		pkt Pkt;
		for (int j = 0; j < wordsInBuffer; j++) {
			Pkt.data[j] = queryWbW[i * wordsInBuffer + j];
			if (i == expectedPackets - 1) {
				Pkt.last = 1;
			} else { Pkt.last = 0;}
			std::cout << Pkt.data[j];
		}
		queryStream.write(Pkt);
	}
	std::cout << std::endl;

	mainFunc(stringStream, queryStream, outputStream);
	while (!outputStream.empty()) {
		outputPkt recPkt;
		outputStream.read(recPkt);
		std::cout << "Mainfunc output: " << recPkt.data.row << " : " << recPkt.data.column << " : " << recPkt.data.score << std::endl;
	}

	int stringWbWint[stringLengthMaxWords];
	int queryWbWint[stringLengthMaxWords];

	for (int i = 0; i < stringLengthMaxWords; i++) {
			stringWbWint[i] = stringWbW[i];
			queryWbWint[i] = queryWbW[i];
		}

	scorePosition highScore = referenceSW(queryWbWint, stringWbWint);
	std::cout << "ref output: " << highScore.row << " : " << highScore.column << " : " << highScore.score << std::endl;
}

int main() {
	//unitTest();
//	std::cout << highestNumberItrs << std::endl;
	completeFunctionality();
	return 0;
}

