#ifndef CONSTANTS_HPP
#define CONSTANTS_HPP

// Vitis libraries
#include "ap_int.h"
#include "hls_stream.h"
#include "ap_axi_sdata.h"
#include <math.h>
#include <hls_vector.h>

#define PRAGMA_SUB(x) _Pragma(#x)
#define DO_PRAGMA(x) PRAGMA_SUB(x)

constexpr int ceilLog2(int x) {
	int retVal = 0;
	if (x == 0 || x == 1) {
		return 0;
	}
	for (int i = 0; i < (sizeof(x)*8); i++) {
		if ((((x - 1) >> i) & 0b01) == 0b01) {
			retVal = i + 1;
		}
	}
	return retVal;
}

constexpr int cPow2(int x) {
	return 0b01 << x;
}

// Constexpressions
constexpr int wordsInBuffer = 4; // buffersize / wordsize
constexpr int wordSize = 32;
constexpr int bufferSize = wordsInBuffer*wordSize;
constexpr int highestNumberItrs = ceilLog2(wordsInBuffer);
constexpr int subScore = 3;
constexpr int gapPenalty = 2;
constexpr int stringLengthMaxWords = 128;
constexpr int CRULatency = 1 * (stringLengthMaxWords + wordsInBuffer + 1);
constexpr int stringLengthMaxWordsPlus1 = stringLengthMaxWords + 1;
constexpr int expectedPackets = stringLengthMaxWords / wordsInBuffer; // stringLengthMaxWords / wordsInBuffer
constexpr int maxScoreBits = ceilLog2(subScore * stringLengthMaxWords);
constexpr int maxScoreBitsPlus1 = maxScoreBits + 1;
//constexpr int amountOfStreams = 4;
//constexpr int partitionedStringLength = stringLengthMaxWords / amountOfStreams;
constexpr int colBitsNeeded = ceilLog2(stringLengthMaxWords);

// typedef for simplicity
#define SLMW CRULatency
#define word ap_uint<wordSize>
#define streamData hls::vector<word, wordsInBuffer>
//#define streamData ap_uint<bufferSize>
#define outStream ap_uint<outputSize>
#define scoreUint ap_uint<maxScoreBits>
#define scoreInt ap_int<maxScoreBitsPlus1>
//#define t_scoreIntVec std::array<scoreUint, stringLengthMaxWordsPlus1>

// Structs
struct scorePosition {
	scoreUint score = 0;
	int row = 0;
	int column = 0;
};

#define scorePosVec hls::vector<scorePosition, wordsInBuffer>

// Streams
typedef hls::axis<streamData, 0, 0, 0> pkt;
typedef hls::axis<scorePosition, 0, 0, 0> outputPkt;

#endif // CONSTANTS_HPP
