#include "calcRow.hpp"

void CRinit(scoreUint scores[wordsInBuffer + 1][stringLengthMaxWordsPlus1]) {
//#pragma HLS INLINE
	for (int i = 1; i < wordsInBuffer + 1; i++) {
#pragma HLS UNROLL
		scores[i][0] = 0;
	}
}

void CRUnstaged(scoreUint scores[wordsInBuffer + 1][stringLengthMaxWordsPlus1], word stringWbW[stringLengthMaxWords], streamData queryWords, scorePosVec& hValues, int rowNumber) {
#pragma HLS PIPELINE II=1
	DO_PRAGMA(HLS LATENCY max=SLMW)

	scorePosVec localHValues;
	for (int i = 1; i < wordsInBuffer + 1; i++) {
#pragma HLS UNROLL
		for (int j = 1; j < stringLengthMaxWordsPlus1; j++) {
#pragma HLS UNROLL
			scoreInt score1 = 0;
			scoreInt score2 = 0;
			scoreInt score3 = 0;
			scoreInt intScore = 0;
			TCalc(score1, scores[i - 1][j]);
			LTCalc(score2, scores[i - 1][j - 1], stringWbW[j - 1], queryWords[i - 1]);
			LCalc(score3, scores[i][j - 1]);
//			std::cout << "Had words: " << stringWbW[j - 1] << " : " << queryWords[i - 1] << " | comparing scores: " << score1 << " : " << score2 << " : " << score3 << std::endl;
			intScore = largestNumber(score1, score2);
			scores[i][j] = largestNumber(intScore, score3);
			if (scores[i][j] > localHValues[i - 1].score) {
				localHValues[i - 1].score = scores[i][j];
				localHValues[i - 1].row = i + rowNumber;
				localHValues[i - 1].column = j;
			}
//			std::cout << scores[i][j] << " | ";
		}
//		std::cout << std::endl;
	}
	hValues = localHValues;
}

void CRStage1(scoreUint scores[wordsInBuffer + 1][stringLengthMaxWordsPlus1], word stringWbW[stringLengthMaxWords], streamData queryWords) {
#pragma HLS PIPELINE II=1
	for (int i = 1; i < wordsInBuffer + 1; i++) {
#pragma HLS UNROLL
		for (int j = 1; j < stringLengthMaxWordsPlus1; j++) {
#pragma HLS UNROLL
			scoreInt score1 = 0;
			scoreInt score2 = 0;
			TCalc(score1, scores[i - 1][j]);
			LTCalc(score2, scores[i - 1][j - 1], stringWbW[j - 1], queryWords[i - 1]);
			if (score1 > score2) {
				scores[i][j] = score1;
			} else {
				scores[i][j] = score2;
			}
		}
	}
}

void CRStage2(scoreUint scores[wordsInBuffer + 1][stringLengthMaxWordsPlus1]) {
//#pragma HLS LATENCY max=1
#pragma HLS PIPELINE II=1
	for (int i = 1; i < wordsInBuffer + 1; i++) {
#pragma HLS UNROLL
		for (int j = 1; j < stringLengthMaxWordsPlus1; j++) {
#pragma HLS UNROLL
			scoreInt score1 = 0;
			LCalc(score1, scores[i][j - 1]);
			if (score1 > scores[i][j]) {
				scores[i][j] = score1;
			}
		}
	}
}

void CRStage3(scoreUint scores[wordsInBuffer + 1][stringLengthMaxWordsPlus1], hls::stream<scorePosVec>& hValueStream, int rowNumber) {
#pragma HLS PIPELINE II=1
	scorePosVec retScores;
	for (int i = 1; i < wordsInBuffer + 1; i++) {
#pragma HLS UNROLL
		scorePosition localHighScore;
		for (int j = 1; j < stringLengthMaxWordsPlus1; j++) {
#pragma HLS UNROLL
			if (scores[i][j] > retScores[i - 1].score) {
				retScores[i - 1].score = scores[i][j];
				retScores[i - 1].row = i + rowNumber;
				retScores[i - 1].column = j;
			}
		}
	}
	hValueStream.write(retScores);
}

void CRend(scoreUint scores[wordsInBuffer + 1][stringLengthMaxWordsPlus1]) {
//#pragma HLS INLINE
	for (int j = 0; j < stringLengthMaxWordsPlus1; j++) {
#pragma HLS UNROLL
		scores[0][j] = scores[wordsInBuffer][j];
	}
}

void calcRows(scorePosVec& hValues, scoreUint scores[wordsInBuffer + 1][stringLengthMaxWordsPlus1], word stringWbW[stringLengthMaxWords], streamData queryWords, int rowNumber) {
#pragma HLS PIPELINE II=1
//#pragma HLS DATAFLOW

//	scorePosVec localHValues;
	CRinit(scores);
//	CRStage1(scores, stringWbW, queryWords);
//	CRStage2(scores);
//	CRStage3(scores, hValueStream, rowNumber);
	CRUnstaged(scores, stringWbW, queryWords, hValues, rowNumber);
	CRend(scores);
}




