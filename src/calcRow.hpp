#ifndef CALCROW_HPP
#define CALCROW_HPP

#include "constants.hpp"
#include "scoreCalc.hpp"

//void calcRow(hls::stream<scorePosition>& hValueStream, t_scoreIntVec scoreInStream, t_scoreIntVec scoreOutStream, word stringWbW[stringLengthMaxWords], word queryWord, int rowNumber);
//void calcRows(hls::stream<scorePosition>& hValueStream, t_scoreIntVec scoreStream[amountOfStreams], word stringWbW[stringLengthMaxWords], streamData queryWords, int rowNumber);
void calcRows(scorePosVec& hValues, scoreUint scores[wordsInBuffer + 1][stringLengthMaxWordsPlus1], word stringWbW[stringLengthMaxWords], streamData queryWords, int rowNumber);

#endif // CALCROW_HPP
