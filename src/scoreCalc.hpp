#ifndef SCORE_CALC_HPP
#define SCORE_CALC_HPP

#include "constants.hpp"

//void scoreCalcArr(scoreUint& retScore, scoreUint& LScore, scoreUint& TScore, scoreUint& LTScore, word queryW, word stringW);
scoreUint scoreCalc(scoreUint& LScore, scoreUint& TScore, scoreUint& LTScore, word queryW, word stringW);
scoreInt largestNumber(scoreInt score1, scoreInt score2);
void LTCalc(scoreInt& retScore, scoreUint& LTScore, word queryW, word stringW);
void LCalc(scoreInt& retScore, scoreUint& LScore);
void TCalc(scoreInt& retScore, scoreUint& TScore);
#endif // SCORE_CALC_HPP
