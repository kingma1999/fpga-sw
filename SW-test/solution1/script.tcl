############################################################
## This file is generated automatically by Vitis HLS.
## Please DO NOT edit it.
## Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
############################################################
open_project SW-test
set_top mainFunc
add_files src/scoreCalc.hpp
add_files src/scoreCalc.cpp
add_files src/main.hpp
add_files src/main.cpp
add_files src/constants.hpp
add_files src/calcRow.hpp
add_files src/calcRow.cpp
add_files -tb src/testBench.cpp -cflags "-Wno-unknown-pragmas" -csimflags "-Wno-unknown-pragmas"
open_solution "solution1" -flow_target vivado
set_part {xc7z045-ffg900-2}
create_clock -period 8 -name default
config_export -format ip_catalog -rtl vhdl -vivado_clock 10
source "./SW-test/solution1/directives.tcl"
csim_design -clean
csynth_design
cosim_design -wave_debug -trace_level all
export_design -flow syn -rtl vhdl -format ip_catalog
